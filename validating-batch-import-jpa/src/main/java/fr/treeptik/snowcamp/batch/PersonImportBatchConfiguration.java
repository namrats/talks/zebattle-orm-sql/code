package fr.treeptik.snowcamp.batch;

import java.util.Arrays;

import javax.persistence.EntityManagerFactory;

import org.modelmapper.ModelMapper;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.database.builder.JpaItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.support.CompositeItemProcessor;
import org.springframework.batch.item.validator.BeanValidatingItemProcessor;
import org.springframework.batch.item.validator.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import fr.treeptik.snowcamp.domain.Person;
import fr.treeptik.snowcamp.web.PersonModel;
import lombok.Setter;

@Configuration
@EnableBatchProcessing
public class PersonImportBatchConfiguration {
	@Autowired @Setter
	private JobBuilderFactory jobBuilderFactory;
	
	@Autowired @Setter
	private StepBuilderFactory stepBuilderFactory;
	
	@Autowired @Setter
	EntityManagerFactory entityManagerFactory;

	@Autowired @Setter
	private ModelMapper mapper;
	
	@Bean
	public FieldSetMapper<PersonModel> personFieldSetMapper() {
		BeanWrapperFieldSetMapper<PersonModel> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
		fieldSetMapper.setTargetType(PersonModel.class);
		return fieldSetMapper;
	}
	
	@Bean
	public FlatFileItemReader<PersonModel> reader() {
		return new FlatFileItemReaderBuilder<PersonModel>()
				.name("personItemReader")
				.resource(new ClassPathResource("persons.csv"))
				.delimited()
				.quoteCharacter('"')
				.names(new String[] { "firstName", "lastName", "email", "address" })
				.fieldSetMapper(personFieldSetMapper())
				.build();
	}
	
	@Bean
	public ItemProcessor<PersonModel, PersonModel> validatingProcessor() {
		return new BeanValidatingItemProcessor<>();
	}
	
	@Bean
	public ItemProcessor<PersonModel, Person> mappingProcessor() {
		ModelMappingProcessor<PersonModel, Person> processor = new ModelMappingProcessor<PersonModel, Person>();
		processor.setDestinationType(Person.class);
		processor.setMapper(mapper);
		return processor;
	}
	
	@Bean
	public ItemProcessor<PersonModel, Person> processor() {
		CompositeItemProcessor<PersonModel, Person> processor = new CompositeItemProcessor<>();
		processor.setDelegates(Arrays.asList(validatingProcessor(), mappingProcessor()));
		return processor;
	}
	
	@Bean
	public JpaItemWriter<Person> writer() {
		return new JpaItemWriterBuilder<Person>()
				.entityManagerFactory(entityManagerFactory)
				.build();
	}
	
	@Bean
	public Job importPersonJob() {
		return jobBuilderFactory.get("importPersonJob")
				.flow(stepBuilderFactory.get("step")
						.<PersonModel, Person>chunk(100)
						.reader(reader())
						.processor(processor())
						.writer(writer())
						.faultTolerant()
						.skipLimit(10)
						.skip(ValidationException.class)
						.build())
				.end()
				.build();
	}
}

package fr.treeptik.snowcamp.batch;

import org.modelmapper.ModelMapper;
import org.springframework.batch.item.ItemProcessor;

import lombok.Setter;

public class ModelMappingProcessor<T, U> implements ItemProcessor<T, U> {
	@Setter
	private ModelMapper mapper;
	
	@Setter
	private Class<U> destinationType;

	@Override
	public U process(T item) throws Exception {
		return mapper.map(item, destinationType);
	}

}

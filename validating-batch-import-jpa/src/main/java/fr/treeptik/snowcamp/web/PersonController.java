package fr.treeptik.snowcamp.web;

import static org.springframework.hateoas.IanaLinkRelations.SELF;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.treeptik.snowcamp.domain.Person;
import fr.treeptik.snowcamp.domain.PersonRepository;
import lombok.Setter;

@Controller
@RequestMapping("persons")
public class PersonController {
	@Autowired @Setter
	private PersonRepository repository;
	
	@Autowired @Setter
	private PersonModelAssembler assembler;
	
	@Autowired @Setter
	private ModelMapper mapper;
	
	@PostMapping
	public ResponseEntity<PersonModel> create(@Valid @RequestBody PersonModel request) {
		Person person = mapper.map(request, Person.class);
		person = repository.save(person);
		
		PersonModel result = assembler.toModel(person);
		return ResponseEntity.created(result.getLink(SELF).get().toUri()).body(result);
	}
	
	@GetMapping
	public ResponseEntity<CollectionModel<PersonModel>> list() {
		List<Person> persons = repository.findAll();
		return ResponseEntity.ok(assembler.toCollectionModel(persons));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<PersonModel> read(@PathVariable Long id) {
		Person person = repository.findById(id).get();
		return ResponseEntity.ok(assembler.toModel(person));
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<PersonModel> update(@PathVariable Long id, @RequestBody PersonModel request) {
		Person person = repository.findById(id).get();
		mapper.map(request, person);
		person = repository.save(person);
		return ResponseEntity.ok(assembler.toModel(person));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Person person = repository.findById(id).get();
		repository.delete(person);
		return ResponseEntity.noContent().build();
	}
}

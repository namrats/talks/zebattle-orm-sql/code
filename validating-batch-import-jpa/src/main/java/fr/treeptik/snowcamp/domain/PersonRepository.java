package fr.treeptik.snowcamp.domain;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.Repository;

public interface PersonRepository extends Repository<Person, Long> {
	Optional<Person> findById(Long id);
	Optional<Person> findByEmail(String email);
	
	List<Person> findAll();
	
	Person save(Person person);
	
	void delete(Person person);
	void deleteAll();
}

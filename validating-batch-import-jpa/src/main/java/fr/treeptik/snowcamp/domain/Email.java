package fr.treeptik.snowcamp.domain;

import javax.persistence.Embeddable;

import org.apache.commons.lang3.Validate;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Embeddable
@EqualsAndHashCode
@ToString
public class Email {
	public static final String VALID_EMAIL =
			"^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$";
	
	private static void checkValidEmail(String value) {
		Validate.matchesPattern(value, VALID_EMAIL);
	}
	
	@Getter
	private String value;
	
	public Email(String value) {
		checkValidEmail(value);
		this.value = value;
	}
}

package fr.treeptik.snowcamp.batch;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import fr.treeptik.snowcamp.domain.Person;
import lombok.Setter;

@SpringBootTest
@DisplayName("Batch person import")
@Tag("batch")
public class PersonBatchTest {
	@Autowired @Setter
	private PersonImportBatch personImportBatch;
	
	@Autowired @Setter
	private JdbcTemplate jdbc;
	
	@Nested
	@DisplayName("given a good file")
	class GivenGoodFile {
		@BeforeEach
		void generateTestFile() throws Exception {
			new ImportFileGenerator().generateValidFile(10_000);
		}
		
		@RepeatedTest(10)
		@DisplayName("good file")
		void goodFile() throws Exception {
			personImportBatch.importPersons();
			
			List<Person> persons = jdbc.query("select * from person", (rs, row) ->
				new Person(
					rs.getLong(1),
					rs.getString(2),
					rs.getString(3),
					rs.getString(4),
					rs.getString(5)));
			
			assertThat(persons, hasSize(10_000));
		}		

		@AfterEach
		void cleanUp() throws Exception {
			jdbc.update("delete from person");
		}
	}
	
	@Nested
	@DisplayName("given a file containing some invalid emails")
	class GivenInvalidEmails {
		@BeforeEach
		void generateTestFile() throws Exception {
			new ImportFileGenerator().generateFileWithSomeInvalidEmails(10_000, 5);
		}
		
		@RepeatedTest(10)
		@DisplayName("when launching the batch then valid entries are imported")
		void thenValidEntriesImported() throws Exception {
			personImportBatch.importPersons();
			
			List<Person> persons = jdbc.query("select * from person", (rs, row) ->
				new Person(
					rs.getLong(1),
					rs.getString(2),
					rs.getString(3),
					rs.getString(4),
					rs.getString(5)));
		
			assertThat(persons, hasSize(10_000));
		}
		
		@AfterEach
		void cleanUp() throws Exception {
			jdbc.update("delete from person");
		}
	}
}

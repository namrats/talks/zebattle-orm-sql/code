package fr.treeptik.snowcamp.batch;

import java.io.FileWriter;
import java.io.IOException;
import java.util.stream.IntStream;

import com.github.javafaker.Faker;

public class ImportFileGenerator {
	private static final String IMPORT_FILE_PATH = "target/persons.csv";

	public static void main(String[] args) {
		try {
			new ImportFileGenerator().generateValidFile(10_000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private final Faker faker = new Faker();
	
	public void generateValidFile(int n) throws Exception {
		try (FileWriter f = new FileWriter(IMPORT_FILE_PATH)) {
			writeHeader(f);
			IntStream.range(0, n).forEach(i -> {
				try {
					f.append(String.format("%s,%s,\"%s\",\"%s\"%n",
							faker.name().firstName(),
							faker.name().lastName(),
							faker.internet().emailAddress(),
							faker.address().fullAddress()));
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			});
		}
	}

	public void generateFileWithSomeInvalidEmails(int validEntryCount, int invalidEntryCount) throws Exception {
		try (FileWriter f = new FileWriter(IMPORT_FILE_PATH)) {
			writeHeader(f);
			IntStream.range(0, validEntryCount).forEach(i -> {
				try {
					f.append(String.format("%s,%s,\"%s\",\"%s\"%n",
							faker.name().firstName(),
							faker.name().lastName(),
							faker.internet().emailAddress(),
							faker.address().fullAddress()));
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			});
			IntStream.range(0, invalidEntryCount).forEach(i -> {
				try {
					f.append(String.format("%s,%s,\"%s\",\"%s\"%n",
							faker.name().firstName(),
							faker.name().lastName(),
							faker.internet().domainName(),
							faker.address().fullAddress()));
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			});
		}
	}

	private void writeHeader(FileWriter f) throws IOException {
		f.append("first_name,last_name,email,address\n");
	}
}

package fr.treeptik.snowcamp;

import java.nio.charset.Charset;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import lombok.Setter;

@Component
public class SchemaUpdater implements ApplicationRunner {
	@Autowired @Setter
	private JdbcTemplate jdbc;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		String script = StreamUtils.copyToString(new ClassPathResource("db/schema.sql").getInputStream(), Charset.defaultCharset());
		jdbc.execute(script);
	}

}

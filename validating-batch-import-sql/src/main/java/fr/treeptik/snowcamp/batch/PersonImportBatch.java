package fr.treeptik.snowcamp.batch;

import java.nio.charset.Charset;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import lombok.Setter;

@Component
public class PersonImportBatch {
	@Autowired @Setter
	private JdbcTemplate jdbc;
	
	public void importPersons() throws Exception {
		String script = StreamUtils.copyToString(new ClassPathResource("db/import_persons.sql").getInputStream(), Charset.defaultCharset());
		jdbc.execute(script);
	}
}

package fr.treeptik.snowcamp.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
public class Person {
	private Long id;
	
	private @NonNull String firstName;
	private @NonNull String lastName;

	private @NonNull String email;
	
	private String address;
}

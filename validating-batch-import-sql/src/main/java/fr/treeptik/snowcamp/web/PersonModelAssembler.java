package fr.treeptik.snowcamp.web;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import fr.treeptik.snowcamp.domain.Person;
import lombok.Setter;

@Component
public class PersonModelAssembler extends RepresentationModelAssemblerSupport<Person, PersonModel> {
	@Autowired @Setter
	private ModelMapper mapper;

	public PersonModelAssembler() {
		super(PersonController.class, PersonModel.class);
	}

	@Override
	public PersonModel toModel(Person entity) {
		PersonModel model = createModelWithId(entity.getId(), entity);
		mapper.map(entity, model);
		return model;
	}
}

package fr.treeptik.snowcamp.web;

import static org.springframework.hateoas.IanaLinkRelations.SELF;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.treeptik.snowcamp.domain.Person;
import lombok.Setter;

@Controller
@RequestMapping("persons")
public class PersonController {
	@Autowired @Setter
	private JdbcTemplate jdbc;
	
	@Autowired @Setter
	private PersonModelAssembler assembler;
	
	@Autowired @Setter
	private ModelMapper mapper;
	
	@PostMapping
	public ResponseEntity<PersonModel> create(@RequestBody PersonModel request) {
		Person person = mapper.map(request, Person.class);
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbc.update(connection -> {
			PreparedStatement statement = connection.prepareStatement("insert into person (first_name, last_name, email, address) values (?, ?, ?, ?)",
					Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, request.getFirstName());
			statement.setString(2, request.getLastName());
			statement.setString(3, request.getEmail());
			statement.setString(4, request.getAddress());
			return statement;
		}, keyHolder);
		
		person.setId(keyHolder.getKey().longValue());
		
		PersonModel result = assembler.toModel(person);
		return ResponseEntity.created(result.getLink(SELF).get().toUri()).body(result);
	}
	
	@GetMapping
	public ResponseEntity<CollectionModel<PersonModel>> list() {
		List<Person> persons = jdbc.query("select id, first_name, last_name, email, address from person",
				this::mapRow);
		return ResponseEntity.ok(assembler.toCollectionModel(persons));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<PersonModel> read(@PathVariable Long id) {
		Person person = jdbc.query("select id, first_name, last_name, email, address from person where id = ?",
			this::extractData,
			id);
		return ResponseEntity.ok(assembler.toModel(person));
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<PersonModel> update(@PathVariable Long id, @RequestBody PersonModel request) {
		Person person = jdbc.query("select id, first_name, last_name, email, address from person where id = ?",
				this::extractData,
				id);
		
		mapper.map(request, person);
		jdbc.update("update person set first_name = ?, last_name = ?, email = ?, address = ? where id = ?", 
				person.getFirstName(),
				person.getLastName(),
				person.getEmail(),
				person.getAddress(),
				person.getId());
		return ResponseEntity.ok(assembler.toModel(person));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		jdbc.update("delete from person where id = ?", id);
		return ResponseEntity.noContent().build();
	}
	
	private Person extractData(ResultSet rs) throws SQLException {
		rs.next();
		return new Person(
				rs.getLong("id"),
				rs.getString("first_name"),
				rs.getString("last_name"),
				rs.getString("email"),
				rs.getString("address"));
	}
	
	private Person mapRow(ResultSet rs, int rowNum) throws SQLException {
		return extractData(rs);
	}
}

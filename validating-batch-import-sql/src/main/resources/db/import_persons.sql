insert into person (first_name, last_name, email, address)
select first_name, last_name, email, address
from csvread('target/persons.csv')
where regexp_like(email, '^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$')
drop table if exists person;

create table person (
  id             bigint       not null auto_increment primary key,
  first_name     varchar(50)  not null,
  last_name      varchar(50)  not null,
  email          varchar(50)  not null,
  address        text
);

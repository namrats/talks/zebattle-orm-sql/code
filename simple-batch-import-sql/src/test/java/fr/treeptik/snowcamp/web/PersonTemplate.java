package fr.treeptik.snowcamp.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import lombok.Setter;

@Component
@Profile("mockmvc")
public class PersonTemplate {
	@Autowired @Setter
	private MockMvc mvc;

	public ResultActions createPerson(JsonObject person) throws Exception {
		return mvc.perform(post("/persons")
				.contentType(MediaType.APPLICATION_JSON)
				.content(person.toString()));
	}
	
	public JsonObject asPerson(ResultActions result) throws Exception {
		String content = result.andReturn().getResponse().getContentAsString();
		try (JsonReader reader = Json.createReader(new StringReader(content))) {
			return reader.readObject();
		}
	}
	
	public JsonObject asPersons(ResultActions result) throws Exception {
		String content = result.andReturn().getResponse().getContentAsString();
		try (JsonReader reader = Json.createReader(new StringReader(content))) {
			return reader.readObject();
		}		
	}
	
	private String selfLink(JsonObject person) {
		return person.getJsonObject("_links").getJsonObject("self").getString("href");
	}

	public ResultActions deletePerson(JsonObject person) throws Exception {
		return mvc.perform(delete(selfLink(person)));
	}

	public JsonObject createAndAssumePerson(JsonObject person) throws Exception {
		return asPerson(createPerson(person).andExpect(status().isCreated()));
	}

	public ResultActions putPerson(JsonObject person) throws Exception {
		return mvc.perform(put(selfLink(person))
				.contentType(MediaType.APPLICATION_JSON)
				.content(person.toString()));
	}

	public ResultActions getPerson(JsonObject person) throws Exception {
		return mvc.perform(get(selfLink(person)));
	}

	public ResultActions listPersons() throws Exception {
		return mvc.perform(get("/persons"));
	}
}

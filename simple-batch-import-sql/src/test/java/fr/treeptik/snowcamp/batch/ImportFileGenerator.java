package fr.treeptik.snowcamp.batch;

import java.io.FileWriter;
import java.io.IOException;
import java.util.stream.IntStream;

import com.github.javafaker.Faker;

public class ImportFileGenerator {
	public static void main(String[] args) {
		try {
			new ImportFileGenerator().run(10_000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private final Faker faker = new Faker();
	
	public void run(int n) throws Exception {
		try (FileWriter f = new FileWriter("target/persons.csv")) {
			f.append("first_name,last_name,email,address\n");
			IntStream.range(0, n).forEach(i -> {
				try {
					f.append(String.format("%s,%s,\"%s\",\"%s\"\n",
							faker.name().firstName(),
							faker.name().lastName(),
							faker.internet().emailAddress(),
							faker.address().fullAddress()));
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			});
		}
	}
}

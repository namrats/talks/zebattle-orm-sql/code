package fr.treeptik.snowcamp.web;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor @AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
@Relation(collectionRelation = "persons")
public class PersonModel extends RepresentationModel<PersonModel> {
	private String firstName;
	private String lastName;
	private String email;
	private String address;
}

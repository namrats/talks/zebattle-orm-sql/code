package fr.treeptik.snowcamp.batch;

import javax.persistence.EntityManagerFactory;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.database.builder.JpaItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import fr.treeptik.snowcamp.domain.Person;
import lombok.Setter;

@Configuration
@EnableBatchProcessing
public class PersonImportBatchConfiguration {
	@Autowired @Setter
	private JobBuilderFactory jobBuilderFactory;
	
	@Autowired @Setter
	private StepBuilderFactory stepBuilderFactory;
	
	@Autowired @Setter
	EntityManagerFactory entityManagerFactory;
	
	@Bean
	public FieldSetMapper<Person> personFieldSetMapper() {
		BeanWrapperFieldSetMapper<Person> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
		fieldSetMapper.setTargetType(Person.class);
		return fieldSetMapper;
	}
	
	@Bean
	public FlatFileItemReader<Person> reader() {
		return new FlatFileItemReaderBuilder<Person>()
				.name("personItemReader")
				.resource(new ClassPathResource("persons.csv"))
				.delimited()
				.quoteCharacter('"')
				.names(new String[] { "firstName", "lastName", "email", "address" })
				.fieldSetMapper(personFieldSetMapper())
				.build();
	}
	
	@Bean
	public JpaItemWriter<Person> writer() {
		return new JpaItemWriterBuilder<Person>()
				.entityManagerFactory(entityManagerFactory)
				.build();
	}
	
	@Bean
	public Job importPersonJob() {
		return jobBuilderFactory.get("importPersonJob")
				.flow(stepBuilderFactory.get("step")
						.<Person, Person>chunk(100)
						.reader(reader())
						.writer(writer())
						.build())
				.end()
				.build();
	}
}

package fr.treeptik.snowcamp.batch;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Tag;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import fr.treeptik.snowcamp.domain.Person;
import fr.treeptik.snowcamp.domain.PersonRepository;
import lombok.Setter;

@SpringBootTest
@SpringBatchTest
@DisplayName("Batch person import")
@Tag("batch")
public class PersonBatchTest {
	@Autowired @Setter
	private JobLauncherTestUtils jobLauncherTestUtils;
	
	@Autowired @Setter
	private PersonRepository personRepository;
	
	@BeforeEach
	void generateTestFile() throws Exception {
		new ImportFileGenerator().run(10_000);
	}
	
	@RepeatedTest(10)
	@DisplayName("good file")
	void goodFile() throws Exception {
		JobExecution job = jobLauncherTestUtils.launchJob();
		
		assertEquals(ExitStatus.COMPLETED, job.getExitStatus());
		
		List<Person> persons = personRepository.findAll();
		assertThat(persons, hasSize(10_000));
	}
	
	@AfterEach
	void cleanUp() throws Exception {
		personRepository.deleteAll();
	}
}

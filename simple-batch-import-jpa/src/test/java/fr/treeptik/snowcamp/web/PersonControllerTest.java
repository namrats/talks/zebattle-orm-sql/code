package fr.treeptik.snowcamp.web;

import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.json.Json;
import javax.json.JsonObject;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.ResultActions;

import com.github.javafaker.Faker;

import lombok.Setter;

@DisplayName("A person controller")
@SpringBootTest
@ActiveProfiles("mockmvc")
@AutoConfigureMockMvc
public class PersonControllerTest {
	@Autowired @Setter
	private PersonTemplate template;
	
	private final Faker faker = new Faker();
	
	private JsonObject fakePerson() {
		return Json.createObjectBuilder()
				.add("firstName", faker.name().firstName())
				.add("lastName", faker.name().lastName())
				.add("email", faker.internet().emailAddress())
				.add("address", faker.address().fullAddress())
				.build();
	}
	
	@Test
	@DisplayName("can create a person")
	void canCreatePerson() throws Exception {
		ResultActions result = template.createPerson(fakePerson());
		
		result.andExpect(status().isCreated());
		JsonObject person = template.asPerson(result);
		assertThat(person, notNullValue());
		
		template.deletePerson(person);
	}

	@Test
	@DisplayName("can delete a person")
	void canDeletePerson() throws Exception {
		ResultActions result = template.createPerson(fakePerson());
		
		result.andExpect(status().isCreated());
		JsonObject person = template.asPerson(result);
		assertThat(person, notNullValue());
		
		template.deletePerson(person)
			.andExpect(status().isNoContent());
	}
	
	@Test
	@DisplayName("full list of people is empty")
	void listEmpty() throws Exception {
		ResultActions result = template.listPersons();
		result.andExpect(status().isOk());
		JsonObject persons = template.asPersons(result);
		assertThat(persons, Matchers.aMapWithSize(0));
	}

	@Nested
	@DisplayName("given a person")
	class GivenAPerson {
		private JsonObject person;
		
		@BeforeEach
		void createPerson() throws Exception {
			person = template.createAndAssumePerson(fakePerson());
		}
		
		@AfterEach
		void deletePerson() throws Exception {
			template.deletePerson(person);
		}
		
		@Test
		@DisplayName("when getting person again then values are unchanged")
		void getPerson() throws Exception {
			ResultActions result = template.getPerson(person);
			result.andExpect(status().isOk());
			
			assertEquals(person, template.asPerson(result));
		}
		
		@Test
		@DisplayName("when putting new values then the person is updated")
		void updatePerson() throws Exception {
			String newFirstName = faker.name().firstName();
			person = Json.createObjectBuilder(person)
					.add("firstName", newFirstName)
					.build();
			ResultActions result = template.putPerson(person);
			result.andExpect(status().isOk());
			
			JsonObject updatedPerson = template.asPerson(result);
			assertEquals(newFirstName, updatedPerson.getString("firstName"));
		}
	}
}

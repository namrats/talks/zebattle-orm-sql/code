package fr.treeptik.snowcamp;

import javax.sql.DataSource;

import org.jooq.DSLContext;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;

@SpringBootApplication
public class OptimisticLockingJooqApplication {

	public static void main(String[] args) {
		SpringApplication.run(OptimisticLockingJooqApplication.class, args);
	}
	
	@Autowired
	private DataSource dataSource;
	
	@Bean
	public DataSourceConnectionProvider connectionProvider() {
		return new DataSourceConnectionProvider(new TransactionAwareDataSourceProxy(dataSource));
	}
	
	@Bean
	public DefaultConfiguration configuration() {
		DefaultConfiguration configuration = new DefaultConfiguration();
		configuration.setConnectionProvider(connectionProvider());
		configuration.settings().withExecuteWithOptimisticLocking(true);

		return configuration;
	}
	
	@Bean
	public DSLContext dsl() {
		return new DefaultDSLContext(configuration());
	}
}

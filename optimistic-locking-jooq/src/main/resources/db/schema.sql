drop table if exists person;

create table person (
  id             bigint         not null auto_increment primary key,
  first_name     varchar(50),
  last_name      varchar(50)  not null,
  REC_TIMESTAMP      timestamp not null
);

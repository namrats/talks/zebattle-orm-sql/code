package fr.treeptik.snowcamp;

import static fr.treeptik.snowcamp.domain.Tables.PERSON;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.jooq.DSLContext;
import org.jooq.exception.DataChangedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import fr.treeptik.snowcamp.domain.tables.records.PersonRecord;

@SpringBootTest
public class OptimisticLockingPersonRecordTest {
	@Autowired
	private DSLContext db;
	
	private Long id;
	
	@BeforeEach
	void insertData() {
		PersonRecord p = db.newRecord(PERSON);
		p.setFirstName("John");
		p.setLastName("Doe");
		p.store();

		id = p.getId();
	}

	@Test
	void optimisticLockingFails() throws Exception {
		PersonRecord p1 = db.selectFrom(PERSON).where(PERSON.ID.eq(id)).fetchOne();
		PersonRecord p2 = db.selectFrom(PERSON).where(PERSON.ID.eq(id)).fetchOne();

		p1.setFirstName("Jane");
		p2.setFirstName("Jim");

		p1.store();

		assertThrows(DataChangedException.class, () -> {
			p2.store();
		});
	}	
}

# Optimistic Locking in JPA

## Prerequisites

- Maven 3.6.0
- Java 11

This project persists data in an H2 database stored in the `target` directory.

## Running tests with "select for update" strategy

```sh
mvn clean test
```

## Running tests for with "REC_TIMESTAMP" strategy

Uncomment the `src/main/resources/db/schema.sql` with **REC_TIMESTAMP** (warning with syntax about coma)

```sh
mvn clean test
```



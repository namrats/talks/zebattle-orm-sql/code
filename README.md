# ZeBattle ORM-SQL Code Samples

[![(c) Treeptik, Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

Code samples comparing ORM and pure SQL implementations of various use cases.

## License

See [License](LICENSE.md).

## Contents

Each use case is specified in a unit test.

- Optimistic locking ([JPA](./optimistic-locking-jpa), [jOOQ](./optimistic-locking-jooq))

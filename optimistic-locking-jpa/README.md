# Optimistic Locking in JPA

## Prerequisites

- Maven 3.6.0
- Java 11

This project persists data in an embedded H2 database.

## Running tests

```sh
mvn clean test
```


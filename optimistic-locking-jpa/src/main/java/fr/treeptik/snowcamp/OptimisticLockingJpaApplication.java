package fr.treeptik.snowcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
@SpringBootApplication
public class OptimisticLockingJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(OptimisticLockingJpaApplication.class, args);
	}

}

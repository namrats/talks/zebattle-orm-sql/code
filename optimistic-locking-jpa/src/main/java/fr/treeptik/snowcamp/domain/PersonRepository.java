package fr.treeptik.snowcamp.domain;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.Repository;

public interface PersonRepository extends Repository<Person, Long> {

	public Person save(Person p);

	@Lock(LockModeType.OPTIMISTIC)
	public Person findById(Long id);

}

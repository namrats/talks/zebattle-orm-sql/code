package fr.treeptik.snowcamp.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Getter @Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class Person {
	@Id @GeneratedValue
	@Setter(AccessLevel.PRIVATE)
	private Long id;
	
	private @NonNull String firstName;
	private @NonNull String lastName;
	
	@Version
	@Getter(AccessLevel.PRIVATE) @Setter(AccessLevel.PRIVATE)
	private Long version;
}

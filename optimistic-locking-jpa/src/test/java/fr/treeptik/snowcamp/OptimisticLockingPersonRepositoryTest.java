package fr.treeptik.snowcamp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.transaction.Transactional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.orm.ObjectOptimisticLockingFailureException;

import fr.treeptik.snowcamp.domain.Person;
import fr.treeptik.snowcamp.domain.PersonRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@SpringBootTest
public class OptimisticLockingPersonRepositoryTest {
	@Autowired
	private PersonRepository repository;
	
	private Long id;
	
	private CountDownLatch latch;
	private ExecutorService executor;
	
	@BeforeEach
	void insertData() {
		Person p = new Person("John", "Doe");
		p = repository.save(p);
		
		id = p.getId();
	}
	
	@BeforeEach
	void setUpParallel() {
		latch = new CountDownLatch(1);
		executor = Executors.newFixedThreadPool(2);
	}

	@Test
	void optimisticLockingFails() throws Exception {
		Future<?> future1 = executor.submit(new FirstNameSetter("Jane"));
		Future<?> future2 = executor.submit(new FirstNameSetter("Jim"));
		
		ExecutionException exception = assertThrows(ExecutionException.class, () -> {
			future1.get(); future2.get();
		});
		
		assertEquals(ObjectOptimisticLockingFailureException.class, exception.getCause().getClass());
	}
	
	@RequiredArgsConstructor
	private final class FirstNameSetter implements Callable<Person> {
		private final @NonNull String firstName;

		@Override
		@Transactional
		public Person call() throws Exception {
			Person p = repository.findById(id);
			
			waitForOthers();
			
			p.setFirstName(firstName);
			
			return repository.save(p);
		}

		private void waitForOthers() throws Exception {
			latch.countDown();
			latch.await();
		}
		
	}
}
